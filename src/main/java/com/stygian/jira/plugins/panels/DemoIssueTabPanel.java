package com.stygian.jira.plugins.panels;

import com.atlassian.crowd.embedded.api.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanel;
import com.atlassian.jira.issue.tabpanels.GenericMessageAction;
import com.atlassian.jira.issue.Issue;
import java.util.Collections;
import java.util.List;

public class DemoIssueTabPanel extends AbstractIssueTabPanel implements IssueTabPanel
{
    private static final Logger log = LoggerFactory.getLogger(DemoIssueTabPanel.class);

    public List getActions(Issue issue, User remoteUser) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<h1>Issue tab panel content</h1>");
        stringBuilder.append("You can generate your html content here to display.");
        return Collections.singletonList(new GenericMessageAction(stringBuilder.toString()));
    }

    public boolean showPanel(Issue issue, User remoteUser)
    {
        return true;
    }
}
